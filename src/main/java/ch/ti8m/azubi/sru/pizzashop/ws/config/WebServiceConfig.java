package ch.ti8m.azubi.sru.pizzashop.ws.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class WebServiceConfig extends Application {
}

